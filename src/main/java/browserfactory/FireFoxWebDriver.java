



package browserfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxWebDriver {
    private static WebDriver driver;

    private FireFoxWebDriver() {
        // private constructor to prevent instantiation outside of this class
    }

    public static WebDriver getSingletonWebDriver() {
        if (driver == null) {
            synchronized (FireFoxWebDriver.class) {
                if (driver == null) {
                    driver = new FirefoxDriver();
                }
            }
        }
        return driver;
    }

    public static WebDriver closeWebDriver(){
        if(driver!=null){
            synchronized(FireFoxWebDriver.class){
                if(driver!=null) {
                    driver.quit();
                }
            }
        }
        return driver;
    }


}
