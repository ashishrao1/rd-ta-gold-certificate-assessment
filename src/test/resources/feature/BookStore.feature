
Feature:


  Scenario Outline:Create user login and validate response
    Given the request body is created with username "<passtestdatauser>" and password "<passtestdatapwd>"
    When I make a POST request to the API endpoint "https://demoqa.com/Account/v1/User"
    Then the status code should be 201
    And the response body validation  with username "<passtestdatauser>"
    Examples:
      | passtestdatauser | passtestdatapwd |
      |Virat_12345678          |  Virat@12345678  |


  Scenario Outline: Web UI Login Validation
    Given I am on the login page of "https://demoqa.com/login"
    When I enter the username "<passtestdatauser>" and password "<passtestdatapwd>"
    And I click on the Login button
    Then I should be redirected to the Books Dashboard page
    And my username should be Displayed
    |Virat_12345|

    Examples:
      | passtestdatauser | passtestdatapwd |
      |Virat_12345       |Virat@12345      |



  Scenario: Get all books information from the bookstore API
    Given I make a GET request to "https://demoqa.com/BookStore/v1/Books"
    When the API call is successful
    Then I capture the details of all the books



