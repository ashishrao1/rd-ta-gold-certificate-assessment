package pojos;

import java.util.List;

public record UserPojo (String userName, String password, List<BooksPojo> books){
}
