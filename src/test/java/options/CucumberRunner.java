
package options;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions( features = {"src/test/resources/feature"},
        glue= "stepdefinitions")
//        plugin= {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}

public class CucumberRunner extends AbstractTestNGCucumberTests {

}


