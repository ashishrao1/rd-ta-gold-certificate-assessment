package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BooksDashBoardPage {

    @FindBy(id= "searchBox")
    private WebElement searchInput;
    @FindBy(id = "userName-value")
    private WebElement userName;
    WebDriver webDriver;
    WebDriverWait wait;
    public BooksDashBoardPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
        this.wait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
    }

    public String verifySearchInput(){
        wait.until(ExpectedConditions.elementToBeClickable(searchInput));
        searchInput.sendKeys("Welcome");
        return searchInput.getText();
    }

    public String verifyUserName(){
        wait.until(ExpectedConditions.elementToBeClickable(userName));
        return userName.getText();
    }


}
