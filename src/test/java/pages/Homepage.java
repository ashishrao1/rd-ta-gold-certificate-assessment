
package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Homepage {
    WebDriver webDriver;

    public Homepage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public List<WebElement> getBookAuthorsAndPublishers(){
        return webDriver.findElements(By.xpath("//div[@class='rt-table']//descendant::div[@role='gridcell' and @style='flex: 100 0 auto; width: 100px;']"));
    }
}