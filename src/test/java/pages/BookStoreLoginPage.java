package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BookStoreLoginPage {


    @FindBy(id="userName")
    private WebElement userNameInput;

    @FindBy(id= "password")
    private WebElement passwordInput;
    @FindBy(id = "login")
    private WebElement loginButton;
    WebDriver webDriver;
    WebDriverWait wait;

    public BookStoreLoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
        this.wait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
    }

    public BookStoreLoginPage launchBooksLoginPage(String url) {
        webDriver.get(url);
        return this;
    }

    public void enterUserDetails(String userName, String password){
        enterUserNameInput(userName);
        enterPasswordInput(password);
    }

    public void verifyUserNameInput(){
        wait.until(ExpectedConditions.elementToBeClickable(userNameInput));
        userNameInput.isDisplayed();
    }

    public void verifyPasswordInput(){
        wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
        passwordInput.isDisplayed();
    }

    public void verifyLoginButton(){
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.isDisplayed();
    }

    public void enterUserNameInput(String userName){
        wait.until(ExpectedConditions.elementToBeClickable(userNameInput));
        userNameInput.sendKeys(userName);
    }

    public void enterPasswordInput(String passWord){
        wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
        passwordInput.sendKeys(passWord);
    }

    public BooksDashBoardPage loginButton(){
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.click();

        return new BooksDashBoardPage(webDriver);
    }





}
