package stepdefinitions;

import browserfactory.WebDriverFactory;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import pages.BookStoreLoginPage;
import pages.BooksDashBoardPage;
import service.AccountV1UserEndPointService;
import service.BooksEndPointService;

import java.util.List;


public class BookStoreLoginFunctionalityFeatureStepDef {
    public WebDriver webDriver;

    public BookStoreLoginPage bookStoreLoginPage;

    public BooksDashBoardPage booksDashBoardPage;
    AccountV1UserEndPointService accountV1UserEndPointService;
    BooksEndPointService booksEndPointService;

    @Before
    public void setUp() {
        webDriver=new WebDriverFactory().createWebDriver("chrome");
        accountV1UserEndPointService= new AccountV1UserEndPointService();
        bookStoreLoginPage=new BookStoreLoginPage(webDriver);
        booksEndPointService=new BooksEndPointService(webDriver);

    }



    @Given("the request body is created with username {string} and password {string}")
    public void theRequestBodyIsCreatedWithUsernameAndPassword(String userName, String password) {
        accountV1UserEndPointService.createRequestBody(userName,password);
    }


    @When("I make a POST request to the API endpoint {string}")
    public void iMakeAPOSTRequestToTheAPIEndpoint(String endPoint) {
        accountV1UserEndPointService.makePostRequest(endPoint);
    }

    @Then("the status code should be {int}")
    public void theStatusCodeShouldBe(int statusCode) {
        Assert.assertEquals(accountV1UserEndPointService.validateStatusCode(),statusCode);

    }

    @And("the response body validation  with username {string}")
    public void theResponseBodyValidationWithUsername(String expectedUserName) {
        Assert.assertEquals(accountV1UserEndPointService.getUserName(),expectedUserName);
    }



    @Given("I am on the login page of {string}")
    public void iAmOnTheLoginPageOf(String url) {
        bookStoreLoginPage.launchBooksLoginPage(url);
    }

    @When("I enter the username {string} and password {string}")
    public void iEnterTheUsernameAndPassword(String username, String password) {
       bookStoreLoginPage.enterUserDetails(username,password);
    }

    @And("I click on the Login button")
    public void iClickOnTheLoginButton() {
        booksDashBoardPage=bookStoreLoginPage.loginButton();
    }

    @Then("I should be redirected to the Books Dashboard page")
    public void iShouldBeRedirectedToTheBooksDashboardPage() {

    }

    @And("my username should be Displayed")
    public void myUsernameShouldBeDisplayed(List<String> userNames) {
        Assert.assertEquals(booksDashBoardPage.verifyUserName(),userNames.get(0));
    }

    @Given("I make a GET request to {string}")
    public void iMakeAGETRequestTo(String basePath) {
        booksEndPointService.getRequestToBooksEndPoint(basePath);
    }

    @When("the API call is successful")
    public void theAPICallIsSuccessful() {
        Assert.assertEquals(booksEndPointService.verifyBooksEndPointResponseStatusCode(),200);
    }

    @Then("I capture the details of all the books")
    public void iCaptureTheDetailsOfAllTheBooks() {
        System.out.println(booksEndPointService.collectAllBooksInformation());
    }




}


