

package service;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tests.Homepage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.path.json.JsonPath.from;

public class BooksEndPointService {

    public RequestSpecification requestSpecification;
    public Response response;

    Homepage homePage;

    public BooksEndPointService(WebDriver webDriver) {
        homePage=new Homepage(webDriver);
    }
    public void getRequestToBooksEndPoint(String basePath) {
        requestSpecification=RestAssured.given();
        response= requestSpecification.request(Method.GET, basePath);
    }

    public int verifyBooksEndPointResponseStatusCode(){
        return response.statusCode();
    }

    public List validatingWebElements(){
        return homePage.getBookAuthorsAndPublishers().stream().map(WebElement::getText).filter(y->!y.equals(" ")).collect(Collectors.toList());
    }


    public List<String> collectAllBooksInformation() {
        List<String> listOfTitles  = response.jsonPath().getList("books.findAll { it.title }.title");
        List<String> listOfAuthors  = response.jsonPath().getList("books.findAll { it.author }.author");
        List<String> listOfPublishers  = response.jsonPath().getList("books.findAll { it.publisher }.publisher");


        List<String> result = new ArrayList<>();
        for(int i = 0; i < listOfTitles.size(); i++) {
            result.add(listOfTitles.get(i));
            result.add(listOfAuthors.get(i));
            result.add(listOfPublishers.get(i));
        }
        return result;
    }

}


