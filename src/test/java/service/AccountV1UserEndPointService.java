
package service;

import groovy.transform.stc.POJO;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pojos.UserPojo;

import static io.restassured.RestAssured.given;

public class AccountV1UserEndPointService {

    public RequestSpecification requestSpecification;
    public Response response;

    public AccountV1UserEndPointService() {

    }
    public void createRequestBody(String userName, String passWord){
         requestSpecification =  RestAssured.given()
                .contentType("application/json")
                .body("{\"userName\": \"" + userName + "\", \"password\": \"" + passWord + "\"}");
    }

    public void makePostRequest(String url) {
        response = requestSpecification.request(Method.POST,url);
    }

    public int validateStatusCode() {
        int actualStatusCode = response.getStatusCode();
        return actualStatusCode;
    }


    public String getUserName(){
        JsonPath jsonPath=response.jsonPath();
        return jsonPath.getString("username");
    }




}
